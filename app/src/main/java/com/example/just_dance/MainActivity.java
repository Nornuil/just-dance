package com.example.just_dance;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.just_dance.retrofit.login.LoginAPI;
import com.example.just_dance.retrofit.login.LoginCallback;
import com.example.just_dance.retrofit.response.LoginResponse;
import com.example.just_dance.utils.CustomProgressDialog;
import com.example.just_dance.utils.SharedPreferencesManager;
import com.google.android.material.textfield.TextInputLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.etUsuarioWrapper)
    TextInputLayout usuarioWrapper;
    @BindView(R.id.etUsuario)
    EditText vUsuario;
    @BindView(R.id.etPasswordWrapper)
    TextInputLayout passwordWrapper;
    @BindView(R.id.etPassword)
    TextView vPass;
    @BindView(R.id.iniciarBtn)
    Button vIniciar;
    @BindView(R.id.registrarseBtn)
    Button vRegistrar;

    private CustomProgressDialog progressDialog = new CustomProgressDialog();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        vIniciar.setOnClickListener(view -> {
            if (isValidForm()) {
                progressDialog.show(MainActivity.this, "Cargando...");

                new LoginAPI().getLogin(vUsuario.getText().toString(), Integer.parseInt(vPass.getText().toString()), this, new LoginCallback() {
                    @Override
                    public void onSuccess(LoginResponse response) {
                        if (response.getData() != null && !response.getData().isEmpty()) {

                            SharedPreferencesManager.getInstance(MainActivity.this).put("mail", vUsuario.getText().toString());
                            SharedPreferencesManager.getInstance(MainActivity.this).putInt("password", Integer.parseInt(vPass.getText().toString()));
                            SharedPreferencesManager.getInstance(MainActivity.this).putInt("id", response.getData().get(0).getId());
                            SharedPreferencesManager.getInstance(MainActivity.this).putInt("estado", response.getData().get(0).getEstado());

                            Toast.makeText(MainActivity.this, "Bienvenido ", Toast.LENGTH_LONG).show();
                            Intent intento = new Intent(MainActivity.this, MenuActivity.class);
                            startActivity(intento);
                            finish();
                        } else {
                            Toast.makeText(MainActivity.this, "Usuario o Contraseña invalido", Toast.LENGTH_LONG).show();
                        }
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onError() {
                        progressDialog.dismiss();
                        Toast.makeText(MainActivity.this, "Mail o Contraseña invalidos", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onNetworkError() {
                        Toast.makeText(MainActivity.this, "El dispositivo no esta conectado a la red", Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }
                });
            }
        });
        vRegistrar.setOnClickListener(view -> {
            Intent intento = new Intent(MainActivity.this, RegistroActivity.class);
            startActivity(intento);
            Toast.makeText(MainActivity.this, "Registrarse", Toast.LENGTH_LONG).show();
        });
    }

    private Boolean isValidForm() {
        boolean validForm = true;
        if (vUsuario.getText().toString().isEmpty()) {
            usuarioWrapper.setErrorEnabled(true);
            usuarioWrapper.setError("El campo Email no puede estar vacío");
            validForm = false;
        } else {
            usuarioWrapper.setErrorEnabled(false);
            usuarioWrapper.setError(null);
        }

        if (vPass.getText().toString().isEmpty()) {
            passwordWrapper.setErrorEnabled(true);
            passwordWrapper.setError("El campo Contraseña no puede estar vacío");
            validForm = false;
        } else {
            passwordWrapper.setErrorEnabled(false);
            passwordWrapper.setError(null);
        }
        return validForm;
    }
}