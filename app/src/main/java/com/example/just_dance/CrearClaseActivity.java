package com.example.just_dance;

import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.just_dance.retrofit.crearClase.CrearClaseAPI;
import com.example.just_dance.retrofit.crearClase.CrearClaseCallback;
import com.example.just_dance.retrofit.response.CrearClaseResponse;
import com.example.just_dance.utils.CustomProgressDialog;
import com.google.android.material.textfield.TextInputLayout;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CrearClaseActivity extends AppCompatActivity {

    @BindView(R.id.cNombre)
    EditText cNombre;
    @BindView(R.id.cAula)
    EditText cAula;
    @BindView(R.id.cDia)
    EditText cDia;
    @BindView(R.id.cHorarioInicio)
    EditText cHorarioInicio;
    @BindView(R.id.cHorarioFin)
    EditText cHorarioFin;
    @BindView(R.id.cVacantes)
    EditText cVacantes;
    @BindView(R.id.cNombreWrapper)
    TextInputLayout cNombreWrapper;
    @BindView(R.id.cAulaWrapper)
    TextInputLayout cAulaWrapper;
    @BindView(R.id.cDiaWrapper)
    TextInputLayout cDiaWrapper;
    @BindView(R.id.cHorarioInicioWrapper)
    TextInputLayout cHorarioInicioWrapper;
    @BindView(R.id.cHorarioFinWrapper)
    TextInputLayout cHorarioFinWrapper;
    @BindView(R.id.cVacantesWrapper)
    TextInputLayout cVacantesWrapper;


    @BindView(R.id.bCrear)
    Button bCrear;

    private int dia = -1;
    private CustomProgressDialog progressDialog = new CustomProgressDialog();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_clase);


        ButterKnife.bind(this);

        ///////////////////////PARA EL HORARIO///////////////////////////
        cHorarioInicio.setInputType(InputType.TYPE_NULL);
        cHorarioFin.setInputType(InputType.TYPE_NULL);

        cHorarioInicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                horario(cHorarioInicio);
            }
        });
        cHorarioFin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                horario(cHorarioFin);
            }
        });

        cDia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAlertDialog();
            }
        });


        bCrear.setOnClickListener(view -> {

            if (isValidForm()) {

                progressDialog.show(CrearClaseActivity.this, "Cargando...");


                new CrearClaseAPI().postAddClase(cNombre.getText().toString(), Integer.parseInt(cAula.getText().toString()), dia, cHorarioInicio.getText().toString(), cHorarioFin.getText().toString(), Integer.parseInt(cVacantes.getText().toString()), this, new CrearClaseCallback() {
                    @Override
                    public void onSuccess(CrearClaseResponse response) {
                        progressDialog.dismiss();
                        Toast.makeText(CrearClaseActivity.this, "Creación Exitosa", Toast.LENGTH_LONG).show();
                        Intent intento = new Intent(CrearClaseActivity.this, MenuActivity.class);
                        startActivity(intento);
                        finish();
                    }

                    @Override
                    public void onExistingClassError() {
                        Toast.makeText(CrearClaseActivity.this, "Horario, Dia y Aula ocupados por otra clase", Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onNetworkError() {
                        Toast.makeText(CrearClaseActivity.this, "El dispositivo no esta conectado a la red", Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onError(String message) {
                        progressDialog.dismiss();
                        Toast.makeText(CrearClaseActivity.this, "Creación Rechazada " + message, Toast.LENGTH_LONG).show();

                    }
                });

            }
// validar que los horarios no esten al reves

        });
    }

    private void horario(EditText campo) {
        final Calendar cldr = Calendar.getInstance();
        int hour = cldr.get(Calendar.HOUR_OF_DAY);
        int minutes = cldr.get(Calendar.MINUTE);
        // time picker dialog
        TimePickerDialog picker = new TimePickerDialog(CrearClaseActivity.this,
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker tp, int sHour, int sMinute) {
                        campo.setText(String.format("%02d", sHour) + ":" + String.format("%02d", sMinute));
                    }
                }, hour, minutes, true);
        picker.show();

    }

    private void showAlertDialog() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(CrearClaseActivity.this);
        alertDialog.setTitle("Elija un Día:");
        String[] items = {"Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"};

        int checkedItem = 0;
        alertDialog.setSingleChoiceItems(items, checkedItem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        cDia.setText(items[0]);
                        dia = 0;
                        break;
                    case 1:
                        cDia.setText(items[1]);
                        dia = 1;
                        break;
                    case 2:
                        cDia.setText(items[2]);
                        dia = 2;
                        break;
                    case 3:
                        cDia.setText(items[3]);
                        dia = 3;
                        break;
                    case 4:
                        cDia.setText(items[4]);
                        dia = 4;
                        break;
                    case 5:
                        cDia.setText(items[5]);
                        dia = 5;
                        break;
                    case 6:
                        cDia.setText(items[6]);
                        dia = 6;
                        break;
                    default:
                        dia = -1;
                        break;

                }
                dialog.dismiss();
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();

    }

    private boolean isValidForm() {

        boolean validForm = true;
        if (cNombre.getText().toString().isEmpty()) {
            cNombreWrapper.setErrorEnabled(true);
            cNombreWrapper.setError("El campo Nombre no puede estar vacío");
            validForm = false;
        } else {
            cNombreWrapper.setErrorEnabled(false);
            cNombreWrapper.setError(null);
        }

        if (cAula.getText().toString().isEmpty()) {
            cAulaWrapper.setErrorEnabled(true);
            cAulaWrapper.setError("El campo Aula no puede estar vacío");
            validForm = false;
        } else {
            cAulaWrapper.setErrorEnabled(false);
            cAulaWrapper.setError(null);
        }

        if (cDia.getText().toString().isEmpty()) {
            cDiaWrapper.setErrorEnabled(true);
            cDiaWrapper.setError("El campo Dia no puede estar vacío");
            validForm = false;
        } else {
            cDiaWrapper.setErrorEnabled(false);
            cDiaWrapper.setError(null);
        }

        if (cDia.getText().toString().isEmpty()) {
            cDiaWrapper.setErrorEnabled(true);
            cDiaWrapper.setError("El campo Dia no puede estar vacío");
            validForm = false;
        } else {
            cDiaWrapper.setErrorEnabled(false);
            cDiaWrapper.setError(null);
        }

        if (cHorarioInicio.getText().toString().isEmpty()) {
            cHorarioInicioWrapper.setErrorEnabled(true);
            cHorarioInicioWrapper.setError("El campo Horario no puede estar vacío");
            validForm = false;
        } else {
            cHorarioInicioWrapper.setErrorEnabled(false);
            cHorarioInicioWrapper.setError(null);
        }

        if (cHorarioFin.getText().toString().isEmpty()) {
            cHorarioFinWrapper.setErrorEnabled(true);
            cHorarioFinWrapper.setError("El campo Horario no puede estar vacío");
            validForm = false;
        } else {
            cHorarioFinWrapper.setErrorEnabled(false);
            cHorarioFinWrapper.setError(null);
        }

        if (cVacantes.getText().toString().isEmpty()) {
            cVacantesWrapper.setErrorEnabled(true);
            cVacantesWrapper.setError("El campo Vacantes no puede estar vacío");
            validForm = false;
        } else {
            if (Integer.parseInt(cVacantes.getText().toString()) < 1) {
                cVacantesWrapper.setErrorEnabled(true);
                cVacantesWrapper.setError("El campo Vacantes debe ser mayor a 0");
                validForm = false;
            } else {
                cVacantesWrapper.setErrorEnabled(false);
                cVacantesWrapper.setError(null);
            }
        }


        /////validar antes q no esten vacios
        Date HorarioInicio = new Date();
        Date HorarioFin = new Date();


        SimpleDateFormat sdf = new java.text.SimpleDateFormat("HH:mm", new Locale("es", "ES"));
        try {
            HorarioInicio = sdf.parse(cHorarioInicio.getText().toString());
            HorarioFin = sdf.parse(cHorarioFin.getText().toString());

            if (HorarioInicio.before(HorarioFin)) {
//positivo
                cHorarioInicioWrapper.setErrorEnabled(false);
                cHorarioInicioWrapper.setError(null);
                cHorarioFinWrapper.setErrorEnabled(false);
                cHorarioFinWrapper.setError(null);

            } else {
                //negativo
                cHorarioInicioWrapper.setErrorEnabled(true);
                cHorarioInicioWrapper.setError("Horario Inicio y Fin mal puesto");
                cHorarioFinWrapper.setErrorEnabled(true);
                cHorarioFinWrapper.setError("Horario Inicio y Fin mal puesto");
                validForm = false;
            }

        } catch (Exception ex) {
            ex.printStackTrace();

        }

        return validForm;
    }


}
