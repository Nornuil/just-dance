package com.example.just_dance;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.AppCompatActivity;

import com.example.just_dance.utils.SharedPreferencesManager;


public class SplashActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        /*aca va el if*/

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                String usuario = SharedPreferencesManager.getInstance(SplashActivity.this).get("mail");//pregunta si hay algun mail en el cache,
                if (usuario != null && !usuario.isEmpty()) {//confirma si esta vacio y si esta null (null nunca esta)
                    Intent intent = (new Intent(SplashActivity.this, MenuActivity.class));//TODO cambiar mainactivity por menuactivity
                    startActivity(intent);
                    finish();
                } else {
                    Intent intent = (new Intent(SplashActivity.this, MainActivity.class));
                    startActivity(intent);
                    finish();
                }
            }
        }, 4000);

    }
}
