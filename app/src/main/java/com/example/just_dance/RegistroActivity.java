package com.example.just_dance;

import android.Manifest;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.example.just_dance.retrofit.registro.RegistroAPI;
import com.example.just_dance.retrofit.registro.RegistroCallback;
import com.example.just_dance.retrofit.response.RegistroResponse;
import com.example.just_dance.utils.CustomProgressDialog;
import com.google.android.material.textfield.TextInputLayout;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegistroActivity extends AppCompatActivity {

    // Binding view with butterKnife
    @BindView(R.id.ptNacimientoWrapper)
    TextInputLayout ptNacimientoWrapper;
    @BindView(R.id.ptNacimiento)
    EditText ptNacimiento;
    @BindView(R.id.bCrear)
    Button bRegistrar;
    @BindView(R.id.ptNombreWrapper)
    TextInputLayout ptNombreWrapper;
    @BindView(R.id.ptNombre)
    EditText ptNombre;
    @BindView(R.id.ptApellidoWrapper)
    TextInputLayout ptApellidoWrapper;
    @BindView(R.id.ptApellido)
    EditText ptApellido;
    @BindView(R.id.ptEmailWrapper)
    TextInputLayout ptEmailWrapper;
    @BindView(R.id.ptEmail)
    EditText ptEmail;
    @BindView(R.id.ptContraseñaWrapper)
    TextInputLayout ptContraseñaWrapper;
    @BindView(R.id.ptContraseña)
    EditText ptContraseña;
    @BindView(R.id.ptContraseña2Wrapper)
    TextInputLayout ptContraseña2Wrapper;
    @BindView(R.id.ptContraseña2)
    EditText ptContraseña2;
    @BindView(R.id.bQrScan)
    Button bQrScan;

    private CustomProgressDialog progressDialog = new CustomProgressDialog();
    private static Calendar calendar;
    private static final int PERMISSION_REQUEST_CODE = 200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);
        ButterKnife.bind(this);

        calendar = Calendar.getInstance();

        ptNacimiento.setOnClickListener(view -> showDatePickerDialog());

        bRegistrar.setOnClickListener(view -> {
            if (isValidForm()) {

                // calendar.getTime();
                ////////////DANDO FORMATO TIPO DATE A DATE SQL///////////////////
                String fecString = ptNacimiento.getText().toString();
                java.sql.Date fecFormatoDate = null;
                try {
                    SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd", new Locale("es", "ES"));

                    fecFormatoDate = new java.sql.Date(sdf.parse(fecString).getTime());
                    System.out.println("Fecha con el formato java.sql.Date: " + fecFormatoDate);
                } catch (Exception ex) {
                    System.out.println("Error al obtener el formato de la fecha/hora: " + ex.getMessage());
                }
                /////////////////FIN////////////////////

                progressDialog.show(RegistroActivity.this, "Cargando...");

                new RegistroAPI().postRegistro(Integer.parseInt(ptContraseña.getText().toString()), ptNombre.getText().toString(), ptApellido.getText().toString(), ptEmail.getText().toString(), fecFormatoDate, this, new RegistroCallback() {
                    @Override
                    public void onSuccess(RegistroResponse response) {
                        progressDialog.dismiss();
                        Toast.makeText(RegistroActivity.this, "Registro Exitoso ", Toast.LENGTH_LONG).show();
                        Intent intento = new Intent(RegistroActivity.this, MainActivity.class);
                        startActivity(intento);
                        finish();
                    }

                    @Override
                    public void onError(String message) {
                        progressDialog.dismiss();
                        Toast.makeText(RegistroActivity.this, "Registro Rechazado " + message, Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void onNetworkError() {
                        Toast.makeText(RegistroActivity.this, "El dispositivo no esta conectado a la red", Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onExistingEmailError() {
                        Toast.makeText(RegistroActivity.this, "Email ya registrado", Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }

                });


            } else {
                //Toast.makeText(RegistroActivity.this, "Registro Incompleto" , Toast.LENGTH_LONG).show();


            }
        });

        bQrScan.setOnClickListener(view -> {
            if (checkPermission()) {
                startQrActivity();
            } else {
                requestPermission();
            }
        });
    }

    private void showDatePickerDialog() {
        DatePickerFragment newFragment = new DatePickerFragment(ptNacimiento);
        newFragment.show(this.getSupportFragmentManager(), "datePicker");
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        private EditText viewUi;

        public DatePickerFragment(EditText view) {
            this.viewUi = view;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            viewUi.setText(year + "-" + (month + 1) + "-" + day);
            calendar.set(year, month - 1, day, 0, 0);
        }
    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    private Boolean isValidForm() {
        boolean validForm = true;
        if (ptNombre.getText().toString().isEmpty()) {
            ptNombreWrapper.setErrorEnabled(true);
            ptNombreWrapper.setError("El campo Nombre no puede estar vacío");
            validForm = false;
        } else {
            ptNombreWrapper.setErrorEnabled(false);
            ptNombreWrapper.setError(null);
        }

        if (ptApellido.getText().toString().isEmpty()) {
            ptApellidoWrapper.setErrorEnabled(true);
            ptApellidoWrapper.setError("El campo Apellido no puede estar vacío");
            validForm = false;
        } else {
            ptApellidoWrapper.setErrorEnabled(false);
            ptApellidoWrapper.setError(null);
        }

        if (ptEmail.getText().toString().isEmpty()) {
            ptEmailWrapper.setErrorEnabled(true);
            ptEmailWrapper.setError("El campo Email no puede estar vacío");
            validForm = false;
        } else {
            if (!isValidEmail(ptEmail.getText())) {
                ptEmailWrapper.setErrorEnabled(true);
                ptEmailWrapper.setError("El formato de Email no es valido");
                validForm = false;
            } else {
                ptEmailWrapper.setErrorEnabled(false);
                ptEmailWrapper.setError(null);
            }

        }

        if (ptNacimiento.getText().toString().isEmpty()) {
            ptNacimientoWrapper.setErrorEnabled(true);
            ptNacimientoWrapper.setError("El campo Nacimiento no puede estar vacío");
            validForm = false;
        } else {
            ptNacimientoWrapper.setErrorEnabled(false);
            ptNacimientoWrapper.setError(null);
        }


        if (ptContraseña.getText().toString().equals(ptContraseña2.getText().toString())) {
            if (ptContraseña.getText().toString().length() < 6 || ptContraseña2.getText().toString().length() < 6) {
                ptContraseñaWrapper.setErrorEnabled(true);
                ptContraseñaWrapper.setError("El campo Contraseña debe contener entre 6 y 8 dígitos");
                ptContraseña2Wrapper.setErrorEnabled(true);
                ptContraseña2Wrapper.setError("El campo Contraseña debe contener entre 6 y 8 dígitos");
                validForm = false;
            } else {
                ptContraseñaWrapper.setErrorEnabled(false);
                ptContraseñaWrapper.setError(null);
                ptContraseña2Wrapper.setErrorEnabled(false);
                ptContraseña2Wrapper.setError(null);
            }
        } else {
            ptContraseñaWrapper.setErrorEnabled(true);
            ptContraseñaWrapper.setError("Las Contraseñas no coinciden");
            ptContraseña2Wrapper.setErrorEnabled(true);
            ptContraseña2Wrapper.setError("Las Contraseñas no coinciden");
            validForm = false;
        }

        return validForm;
    }

    private void startQrActivity() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //MENSAJE
        builder.setMessage("Para utilizar esta función es necesario escanear el código QR ubicado en la parte inferior derecha del documento.");
        builder.setTitle("QR Scanner");
        builder.setNeutralButton("Aceptar", (dialog, which) -> {
            Intent intent = new Intent(RegistroActivity.this, QrCodeScanner.class);
            startActivityForResult(intent, QrCodeScanner.VALUE_QR_CODE);
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private boolean checkPermission() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.CAMERA},
                PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startQrActivity();
            } else {
                Toast.makeText(RegistroActivity.this, "Debe otorgar los permisos necesarios para poder utilizar esta acción", Toast.LENGTH_LONG).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == QrCodeScanner.VALUE_QR_CODE && data != null && data.hasExtra(QrCodeScanner.KEY_QR_CODE)) {
            String result = data.getStringExtra(QrCodeScanner.KEY_QR_CODE);
            if (result != null) {
                String[] dataParsed = result.split("@");

                String dniApellido;
                String dniNombre;
                String dniNacimiento;

                if (dataParsed.length < 10) {
                    //Datos que pueden obtenerse del código QR
                    //String dniTramite = dataParsed[0];
                    dniApellido = dataParsed[1];
                    dniNombre = dataParsed[2];
                    //String dniGenero = dataParsed[3];
                    //String dniDocumento = dataParsed[4];
                    //String dniEjemplar = dataParsed[5];
                    dniNacimiento = dataParsed[6];
                    //String dniEmision = dataParsed[7];
                } else {
                    dniApellido = dataParsed[4];
                    dniNombre = dataParsed[5];
                    dniNacimiento = dataParsed[7];
                }


                ptNombre.setText(dniNombre);
                ptApellido.setText(dniApellido);

                //En el documento viene "DD/MM/YYYY" mientras que el formato esperado es "YYYY-MM-DD"
                String[] nacimientoParsed = dniNacimiento.split("/");
                String fechaNacimiento = nacimientoParsed[2] + "-" + nacimientoParsed[1] + "-" + nacimientoParsed[0];
                ptNacimiento.setText(fechaNacimiento);
            }
        }
    }
}