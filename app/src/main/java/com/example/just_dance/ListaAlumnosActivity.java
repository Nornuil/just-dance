package com.example.just_dance;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.just_dance.adapter.AlumnoAdapterListener;
import com.example.just_dance.adapter.AlumnosAdapter;
import com.example.just_dance.model.ListaAlumnosModel;
import com.example.just_dance.retrofit.actualizarEstado.ActualizarEstadoAPI;
import com.example.just_dance.retrofit.actualizarEstado.ActualizarEstadoCallback;
import com.example.just_dance.retrofit.editarPerfil.EditarPerfilAPI;
import com.example.just_dance.retrofit.editarPerfil.EditarPerfilCallback;
import com.example.just_dance.retrofit.listaAlumnos.ListaAlumnosAPI;
import com.example.just_dance.retrofit.listaAlumnos.ListaAlumnosCallback;
import com.example.just_dance.retrofit.resetPass.ResetPassAPI;
import com.example.just_dance.retrofit.resetPass.ResetPassCallback;
import com.example.just_dance.retrofit.response.ActualizarEstadoResponse;
import com.example.just_dance.retrofit.response.EditarPerfilResponse;
import com.example.just_dance.retrofit.response.ListaAlumnosResponse;
import com.example.just_dance.retrofit.response.ResetPassResponse;
import com.example.just_dance.utils.CustomProgressDialog;
import com.example.just_dance.utils.SharedPreferencesManager;
import com.example.just_dance.utils.Utils;
import com.google.android.material.textfield.TextInputLayout;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ListaAlumnosActivity extends AppCompatActivity {

    @BindView(R.id.RecyclerID)
    RecyclerView recycler;

    EditText cNombre;
    EditText cApellido;
    EditText cEmail;
    EditText cNacimiento;
    TextInputLayout cNombreWrapper;
    TextInputLayout cApellidoWrapper;
    TextInputLayout cEmailWrapper;
    TextInputLayout cNacimientoWrapper;


    //////
    private static Calendar calendar;
    //////
    private CustomProgressDialog progressDialog = new CustomProgressDialog();
    private AlumnosAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_alumnos);
        ButterKnife.bind(this);
//////////
        calendar = Calendar.getInstance();
//////////


        recycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new AlumnosAdapter(new ArrayList<>(), this, new AlumnoAdapterListener() {
            @Override
            public void editarEstado(ListaAlumnosModel alumno, int position) {
                ListaAlumnosActivity.this.editarEstado(alumno, position);
            }

            @Override
            public void cambioPerfil(ListaAlumnosModel alumno, int position) {
                ListaAlumnosActivity.this.editarPerfil(alumno, position);
            }

            @Override
            public void resetPass(ListaAlumnosModel alumno, int position) {
                ListaAlumnosActivity.this.resetPass(alumno, position);
            }
        });
        recycler.setAdapter(adapter);
        progressDialog.show(ListaAlumnosActivity.this, "Cargando...");

        new ListaAlumnosAPI().getListaAlumnos(this, new ListaAlumnosCallback() {
            @Override
            public void onSuccess(ListaAlumnosResponse response) {
                adapter.updateData(response.getListaUsuarios());
                progressDialog.dismiss();
            }

            @Override
            public void onError() {
                progressDialog.dismiss();
                Toast.makeText(ListaAlumnosActivity.this, "Hubo un error", Toast.LENGTH_LONG).show();
                finish();
            }
        });


    }


    private void editarEstado(ListaAlumnosModel alumno, int position) {

        Log.e("ListaAlumno", "item: " + position);
        showAlertDialog(alumno, position);
        //Actualizar los valores de alumno
        //adapter.updateItem(alumno,position);
    }

    private void resetPass(ListaAlumnosModel alumno, int position) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //MENSAJE
        builder.setMessage("¿Estás seguro?");
        builder.setTitle("Resetear Contraseña");

        builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                Log.e("ListaAlumno", "item: " + position);
                progressDialog.show(ListaAlumnosActivity.this, "Reseteando...");

                new ResetPassAPI().getResetPass(alumno.getId(), ListaAlumnosActivity.this, new ResetPassCallback() {
                    @Override
                    public void onSuccess(ResetPassResponse response) {
                        Toast.makeText(ListaAlumnosActivity.this, "Reset Exitoso", Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onError() {
                        Toast.makeText(ListaAlumnosActivity.this, "Reset Rechazado", Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }

                    @Override
                    public void onNetworkError() {
                        Toast.makeText(ListaAlumnosActivity.this, "El dispositivo no esta conectado a la red", Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }
                });
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();


    }

    private void editarPerfil(ListaAlumnosModel alumno, int position) {


        final AlertDialog dialogBuilder = new AlertDialog.Builder(this).create();
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_editar_perfil, null);

        cNombre = (EditText) dialogView.findViewById(R.id.cNombre);
        cApellido = (EditText) dialogView.findViewById(R.id.cApellido);
        cEmail = (EditText) dialogView.findViewById(R.id.cEmail);
        cNacimiento = (EditText) dialogView.findViewById(R.id.cNacimiento);
        Button bAceptar = (Button) dialogView.findViewById(R.id.bAceptar);
        Button bCancelar = (Button) dialogView.findViewById(R.id.bCancelar);
        cNombreWrapper = dialogView.findViewById(R.id.cNombreWrapper);
        cApellidoWrapper = dialogView.findViewById(R.id.cApellidoWrapper);
        cEmailWrapper = dialogView.findViewById(R.id.cEmailWrapper);
        cNacimientoWrapper = dialogView.findViewById(R.id.cNacimientoWrapper);

////////////
        cNacimiento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.getInstance().showDatePickerDialog(cNacimiento, ListaAlumnosActivity.this);
            }
        });

////////////


        bCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogBuilder.dismiss();
            }
        });
        bAceptar.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {

                if (isValidForm()) {

                    //1 mostrar progress bar
                    progressDialog.show(ListaAlumnosActivity.this, "Cargando...");
                    //2 llamar al servicio

                    Date nuevaFecha;

                    try {
                        SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd", new Locale("es", "ES"));
                        nuevaFecha = new Date(sdf.parse(cNacimiento.getText().toString()).getTime());
                    } catch (ParseException ex) {
                        nuevaFecha = new Date(new java.util.Date().getTime());
                        //java.util.Date es el Date solo fecha,horario,pm TIMESTAMP
                        //java.sql.Date es solo fecha YYYY/MM/DD
                    }

                    new EditarPerfilAPI().getEditarPerfil(cNombre.getText().toString(), cApellido.getText().toString(), cEmail.getText().toString(), nuevaFecha, alumno.getId(), ListaAlumnosActivity.this, new EditarPerfilCallback() {
                        @Override
                        public void onSuccess(EditarPerfilResponse response) {
                            //3A en el onSuccess actualizar los valores alumno y update item en el adapter
                            //ejemplo: alumno.setNombre(cNombre.getText().toString); hacer con todos los campos
                            alumno.setNombre(cNombre.getText().toString());
                            alumno.setApellido(cApellido.getText().toString());
                            alumno.setEmail(cEmail.getText().toString());
                            alumno.setFechaStr(cNacimiento.getText().toString());
                            //adapter.updateItem(alumno,position) en el onSuccess
                            adapter.updateItem(alumno, position);
                            //3 mostrar toast con respuesta (exitosa o fallida)
                            Toast.makeText(ListaAlumnosActivity.this, "Actualizacion Exitosa", Toast.LENGTH_LONG).show();
                            //4 ocultar progress bar
                            progressDialog.dismiss();
                            dialogBuilder.dismiss();//poner luego del dismiss onSuccess
                        }

                        @Override
                        public void onError() {
                            //4 ocultar progress bar
                            progressDialog.dismiss();
                            //3 mostrar toast con respuesta (exitosa o fallida)
                            Toast.makeText(ListaAlumnosActivity.this, "Error del servidor", Toast.LENGTH_LONG).show();

                        }

                        @Override
                        public void onNetworkError() {
                            Toast.makeText(ListaAlumnosActivity.this, "El dispositivo no esta conectado a la red", Toast.LENGTH_LONG).show();
                            progressDialog.dismiss();
                        }

                        @Override
                        public void onEmailError() {
                            Toast.makeText(ListaAlumnosActivity.this, "El Email ya esta registrado", Toast.LENGTH_LONG).show();
                            progressDialog.dismiss();
                        }

                    });

                }
            }


        });

        cNombre.setText(alumno.getNombre());
        cApellido.setText(alumno.getApellido());
        cEmail.setText(alumno.getEmail());
        cNacimiento.setText(alumno.getFechaStr());

        dialogBuilder.setView(dialogView);
        dialogBuilder.show();


    }


    private void actualizarEstado(int nuevoEstado, ListaAlumnosModel alumno, int position) { //servicio es el estado nuevo
        progressDialog.show(ListaAlumnosActivity.this, "Cargando...");
        new ActualizarEstadoAPI().putEstado(alumno.getId(), nuevoEstado, ListaAlumnosActivity.this, new ActualizarEstadoCallback() {
            @Override
            public void onSuccess(ActualizarEstadoResponse response) {
                alumno.setEstado(nuevoEstado);
                adapter.updateItem(alumno, position);
                progressDialog.dismiss();
                Toast.makeText(ListaAlumnosActivity.this, "Cambio exitoso", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onError() {
                progressDialog.dismiss();
                Toast.makeText(ListaAlumnosActivity.this, "Cambio rechazado", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNetworkError() {
                Toast.makeText(ListaAlumnosActivity.this, "El dispositivo no esta conectado a la red", Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
            }
        });

    }

    public static boolean isValidEmail(CharSequence target) {
        return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    private Boolean isValidForm() {


        boolean validForm = true;
        if (cNombre.getText().toString().isEmpty()) {
            cNombreWrapper.setErrorEnabled(true);
            cNombreWrapper.setError("El campo Nombre no puede estar vacío");
            validForm = false;
        } else {
            cNombreWrapper.setErrorEnabled(false);
            cNombreWrapper.setError(null);
        }

        if (cApellido.getText().toString().isEmpty()) {
            cApellidoWrapper.setErrorEnabled(true);
            cApellidoWrapper.setError("El campo Apellido no puede estar vacío");
            validForm = false;
        } else {
            cApellidoWrapper.setErrorEnabled(false);
            cApellidoWrapper.setError(null);
        }

        if (cEmail.getText().toString().isEmpty()) {
            cEmailWrapper.setErrorEnabled(true);
            cEmailWrapper.setError("El campo Email no puede estar vacío");
            validForm = false;
        } else {
            if (!isValidEmail(cEmail.getText())) {
                cEmailWrapper.setErrorEnabled(true);
                cEmailWrapper.setError("El formato de Email no es valido");
                validForm = false;
            } else {
                cEmailWrapper.setErrorEnabled(false);
                cEmailWrapper.setError(null);
            }

        }

        if (cNacimiento.getText().toString().isEmpty()) {
            cNacimientoWrapper.setErrorEnabled(true);
            cNacimientoWrapper.setError("El campo Nacimiento no puede estar vacío");
            validForm = false;
        } else {

            cNacimientoWrapper.setErrorEnabled(false);
            cNacimientoWrapper.setError(null);
        }

        return validForm;
    }


    private void showAlertDialog(ListaAlumnosModel alumno, int position) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(ListaAlumnosActivity.this);
        alertDialog.setTitle("Elija un estado:");
        String[] items;
        if (SharedPreferencesManager.getInstance(this).getInt("estado") == 3) {
            items = new String[]{"Inactivo", "Alumno", "Administrativo", "Director"};
        } else {
            items = new String[]{"Inactivo", "Alumno"};
        }
        int checkedItem = alumno.getEstado();
        alertDialog.setSingleChoiceItems(items, checkedItem, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        // Toast.makeText(ListaAlumnosActivity.this, "Clicked on Inactivo", Toast.LENGTH_LONG).show();
                        actualizarEstado(0, alumno, position);
                        break;
                    case 1:
                        // Toast.makeText(ListaAlumnosActivity.this, "Clicked on Alumno", Toast.LENGTH_LONG).show();
                        actualizarEstado(1, alumno, position);
                        break;
                    case 2:
                        // Toast.makeText(ListaAlumnosActivity.this, "Clicked on Administrativo", Toast.LENGTH_LONG).show();
                        actualizarEstado(2, alumno, position);
                        break;
                    case 3:
                        // Toast.makeText(ListaAlumnosActivity.this, "Clicked on Director", Toast.LENGTH_LONG).show();
                        actualizarEstado(3, alumno, position);
                        break;

                }
                dialog.dismiss();
            }
        });
        AlertDialog alert = alertDialog.create();
        alert.setCanceledOnTouchOutside(false);
        alert.show();

    }
}