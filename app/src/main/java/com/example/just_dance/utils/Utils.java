package com.example.just_dance.utils;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import java.util.Calendar;

public class Utils {

    private static volatile Utils singletonInstance = new Utils();

    private Utils() {
    }

    public static Utils getInstance() {
        return singletonInstance;
    }

    public void showDatePickerDialog(EditText view, AppCompatActivity context) {
        DatePickerFragment newFragment = new DatePickerFragment(view);
        newFragment.show(context.getSupportFragmentManager(), "datePicker");
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        private EditText viewUi;
        private static Calendar calendar = Calendar.getInstance();

        public DatePickerFragment(EditText view) {
            this.viewUi = view;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            String dateSelected = year + "-" + (month + 1) + "-" + day;
            viewUi.setText(dateSelected);
            calendar.set(year, month - 1, day, 0, 0);
        }
    }

    public boolean isNetworkConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm != null && cm.getActiveNetwork() != null &&
                cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

}
