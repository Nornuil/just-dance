package com.example.just_dance.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.BlendMode;
import android.graphics.BlendModeColorFilter;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;

import com.example.just_dance.R;

public class CustomProgressDialog {
    private CustomDialog dialog;

    public void dismiss() {
        dialog.dismiss();
    }


    public Dialog show(Context context, CharSequence title) {
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View view = inflater.inflate(R.layout.progress_dialog_view, null);

        TextView textViewTitle = view.findViewById(R.id.cp_title);
        if (textViewTitle != null) {
            if (title != null) {
                textViewTitle.setText(title);
                textViewTitle.setTextColor(Color.WHITE);
            } else {
                textViewTitle.setVisibility(View.GONE);
            }
        }
        View cardView = view.findViewById(R.id.cp_cardview);
        if (cardView != null) {
            cardView.setBackgroundColor(Color.parseColor("#70000000"));
        }
        ProgressBar progressBar = view.findViewById(R.id.cp_pbar);
        if (progressBar != null) {
            setColorFilter(progressBar.getIndeterminateDrawable(), ResourcesCompat.getColor(((Activity) context).getResources(), R.color.colorPrimary, null));
        }
        dialog = new CustomDialog((Context) context);
        dialog.setContentView(view);
        dialog.show();
        return dialog;
    }

    private void setColorFilter(Drawable drawable, int color) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            drawable.setColorFilter(new BlendModeColorFilter(color, BlendMode.SRC_ATOP));
        } else {
            drawable.setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        }
    }


    private static class CustomDialog extends Dialog {
        public CustomDialog(@NonNull Context context) {
            super(context, R.style.CustomDialogTheme);
            if (getWindow() != null) {
                if (getWindow().getDecorView().getRootView() != null) {
                    getWindow().getDecorView().getRootView().setBackgroundResource(R.color.dialogBackground);
                }
                getWindow().getDecorView().setOnApplyWindowInsetsListener((v, insets) -> insets.consumeSystemWindowInsets());
            }
        }
    }
}
