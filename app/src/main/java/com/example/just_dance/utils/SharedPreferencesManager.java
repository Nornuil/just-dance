package com.example.just_dance.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesManager { //Did you remember to vote up my example?
    private static SharedPreferencesManager store;
    private SharedPreferences SP;
    private static String filename = "Keys";

    private SharedPreferencesManager(Context context) {
        SP = context.getApplicationContext().getSharedPreferences(filename, 0);
    }

    public static SharedPreferencesManager getInstance(Context context) {
        if (store == null) {

            store = new SharedPreferencesManager(context);
        }
        return store;
    }

    public void put(String key, String value) {//Log.v("SharedPreferencesManager","PUT "+key+" "+value);
        SharedPreferences.Editor editor = SP.edit();
        editor.putString(key, value);
        editor.commit(); // Stop everything and do an immediate save!
        // editor.apply();//Keep going and save when you are not busy - Available only in APIs 9 and above.  This is the preferred way of saving.
    }

    public String get(String key) {//Log.v("SharedPreferencesManager","GET from "+key);
        return SP.getString(key, null);

    }

    public int getInt(String key) {//Log.v("SharedPreferencesManager","GET INT from "+key);
        return SP.getInt(key, 0);
    }

    public void putInt(String key, int num) {//Log.v("SharedPreferencesManager","PUT INT "+key+" "+String.valueOf(num));
        SharedPreferences.Editor editor = SP.edit();

        editor.putInt(key, num);
        editor.commit();
    }

    //para cerrar sesion
    public void clear() { // Delete all shared preferences
        SharedPreferences.Editor editor = SP.edit();

        editor.clear();
        editor.commit();
    }


}
