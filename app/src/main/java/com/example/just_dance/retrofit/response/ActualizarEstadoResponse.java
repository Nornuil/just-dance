package com.example.just_dance.retrofit.response;

import com.google.gson.annotations.SerializedName;

public class ActualizarEstadoResponse {
    @SerializedName("actualizar")
    private int actualiza;

    public ActualizarEstadoResponse(int ID) {
        this.actualiza = ID;

    }

    public int getID() {
        return actualiza;
    }

    public void setID(int ID) {
        this.actualiza = ID;
    }
}
