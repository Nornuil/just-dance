package com.example.just_dance.retrofit.registro;


import com.example.just_dance.retrofit.response.RegistroResponse;

public interface RegistroCallback {
    void onSuccess(RegistroResponse response);

    void onError(String message);

    void onExistingEmailError();

    void onNetworkError();
}
