package com.example.just_dance.retrofit.actualizarPass;

import com.example.just_dance.retrofit.response.ActualizarPassResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ActualizarPassInterface {
    @GET("/actualizarpass")
    Call<ActualizarPassResponse> getActualizarPass(@Query("ID") int ID,
                                                   @Query("Password") int Password);
}
