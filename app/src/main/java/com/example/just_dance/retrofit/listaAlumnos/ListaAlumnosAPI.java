package com.example.just_dance.retrofit.listaAlumnos;

import android.content.Context;

import com.example.just_dance.retrofit.ApiClient;
import com.example.just_dance.retrofit.response.ListaAlumnosResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListaAlumnosAPI {

    public void getListaAlumnos(Context context, final ListaAlumnosCallback callback) {
        ListaAlumnosInterface apiService = ApiClient.getClient().create(ListaAlumnosInterface.class);
        Call<ListaAlumnosResponse> call = apiService.getListaAlumnos();
        call.enqueue(new Callback<ListaAlumnosResponse>() {
            @Override
            public void onResponse(Call<ListaAlumnosResponse> call, Response<ListaAlumnosResponse> response) {
                if (response.isSuccessful()) {
                    callback.onSuccess(response.body());
                } else {
                    callback.onError();
                }
            }

            @Override
            public void onFailure(Call<ListaAlumnosResponse> call, Throwable t) {
                callback.onError();
            }
        });
    }


}
