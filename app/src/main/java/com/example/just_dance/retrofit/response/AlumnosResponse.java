package com.example.just_dance.retrofit.response;
import com.example.just_dance.model.AlumnosModel;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AlumnosResponse {
    @SerializedName("usuarios")
    private List<AlumnosModel> lUsuarios;

    public AlumnosResponse(List<AlumnosModel> lUsuarios) {
        this.lUsuarios = lUsuarios;
    }

    public List<AlumnosModel> getlUsuarios() {
        return lUsuarios;
    }

    public void setlUsuarios(List<AlumnosModel> lUsuarios) {
        this.lUsuarios = lUsuarios;
    }
}
