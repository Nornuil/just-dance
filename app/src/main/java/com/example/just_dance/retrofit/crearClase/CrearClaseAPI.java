package com.example.just_dance.retrofit.crearClase;

import android.content.Context;

import com.example.just_dance.retrofit.ApiClient;
import com.example.just_dance.retrofit.response.CrearClaseResponse;
import com.example.just_dance.utils.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CrearClaseAPI {

    public void postAddClase(String Nombre, int Aula, int Dia, String HorarioInicio, String HorarrioFin, int Vacantes, Context context, final CrearClaseCallback callback) {

        if (Utils.getInstance().isNetworkConnected(context)) {

            CrearClaseInterface apiService = ApiClient.getClient().create(CrearClaseInterface.class);
            Call<CrearClaseResponse> call = apiService.getAddClase(Nombre, Aula, Dia, HorarioInicio, HorarrioFin, Vacantes);
            call.enqueue(new Callback<CrearClaseResponse>() {
                @Override
                public void onResponse(Call<CrearClaseResponse> call, Response<CrearClaseResponse> response) {
                    if (response.isSuccessful()) {

                        switch (response.body().getCrear()) {

                            case 0:
                                callback.onExistingClassError();
                                break;
                            case 1:
                                callback.onSuccess(response.body());
                                break;
                            default:
                                callback.onError("Codigo de Error: " + response.code());
                        }


                    } else {
                        callback.onError("Codigo de Error: " + response.code());
                    }
                }

                @Override
                public void onFailure(Call<CrearClaseResponse> call, Throwable t) {
                    callback.onError(t.getMessage());
                }
            });

        } else {
            callback.onNetworkError();
        }


    }
}
