package com.example.just_dance.retrofit.registro;

import android.content.Context;

import com.example.just_dance.retrofit.ApiClient;
import com.example.just_dance.retrofit.response.RegistroResponse;
import com.example.just_dance.utils.Utils;

import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistroAPI {

    public void postRegistro(int Password, String Nombre, String Apellido, String Email, Date Fecha_Nacimiento, Context context, final RegistroCallback callback) {

        if (Utils.getInstance().isNetworkConnected(context)) {

            RegistroInterface apiService = ApiClient.getClient().create(RegistroInterface.class);
            Call<RegistroResponse> call = apiService.postAddAlumno(Password, Nombre, Apellido, Email, Fecha_Nacimiento);
            call.enqueue(new Callback<RegistroResponse>() {
                @Override
                public void onResponse(Call<RegistroResponse> call, Response<RegistroResponse> response) {
                    if (response.isSuccessful()) {

                        switch (response.body().getRegistro()) {

                            case 0:
                                callback.onExistingEmailError();
                                break;
                            case 1:
                                callback.onSuccess(response.body());
                                break;
                            default:
                                callback.onError("Codigo de Error: " + response.code());
                        }


                    } else {
                        callback.onError("Codigo de Error: " + response.code());
                    }
                }

                @Override
                public void onFailure(Call<RegistroResponse> call, Throwable t) {
                    callback.onError(t.getMessage());
                }
            });

        } else {
            callback.onNetworkError();
        }
    }

}
