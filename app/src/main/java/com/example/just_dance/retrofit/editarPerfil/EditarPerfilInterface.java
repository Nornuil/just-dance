package com.example.just_dance.retrofit.editarPerfil;

import com.example.just_dance.retrofit.response.EditarPerfilResponse;

import java.util.Date;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface EditarPerfilInterface {
    @GET("/editarperfil")
    Call<EditarPerfilResponse> getEditarPerfil(@Query("Nombre") String nombre,
                                               @Query("Apellido") String apellido,
                                               @Query("Email") String email,
                                               @Query("Fecha_Nacimiento") Date nacimiento,
                                               @Query("ID") int id);
}
