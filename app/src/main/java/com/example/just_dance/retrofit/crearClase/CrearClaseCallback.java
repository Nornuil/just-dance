package com.example.just_dance.retrofit.crearClase;

import com.example.just_dance.retrofit.response.CrearClaseResponse;


public interface CrearClaseCallback {
    void onSuccess(CrearClaseResponse response);

    void onError(String message);

    void onNetworkError();

    void onExistingClassError();
    //Otro error que se ocurra
}
