package com.example.just_dance.retrofit.perfil;

import android.content.Context;

import com.example.just_dance.retrofit.ApiClient;
import com.example.just_dance.retrofit.response.MiPefilResponse;
import com.example.just_dance.utils.SharedPreferencesManager;
import com.example.just_dance.utils.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MiPerfilAPI {


    public void getMiPerfil(Context context, final MiPerfilCallback callback) {
        if (Utils.getInstance().isNetworkConnected(context)) {

            MiPerfilInterface apiService = ApiClient.getClient().create(MiPerfilInterface.class);
            Call<MiPefilResponse> call = apiService.getMiPerfil(SharedPreferencesManager.getInstance(context).getInt("id"));
            call.enqueue(new Callback<MiPefilResponse>() {
                @Override
                public void onResponse(Call<MiPefilResponse> call, Response<MiPefilResponse> response) {
                    if (response.isSuccessful()) {
                        callback.onSuccess(response.body());
                    } else {
                        callback.onError();
                    }
                }

                @Override
                public void onFailure(Call<MiPefilResponse> call, Throwable t) {
                    callback.onError();
                }
            });

        } else {
            callback.onNetworkError();
        }
    }
}

