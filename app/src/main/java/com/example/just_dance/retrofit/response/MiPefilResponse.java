package com.example.just_dance.retrofit.response;

import com.example.just_dance.model.MiPerfilModel;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MiPefilResponse {
    @SerializedName("usuarios")
    private List<MiPerfilModel> perfil;


    public MiPefilResponse(List<MiPerfilModel> perfil) {
        this.perfil = perfil;
    }

    public List<MiPerfilModel> getPerfil() {
        return perfil;
    }

    public void setPerfil(List<MiPerfilModel> perfil) {
        this.perfil = perfil;
    }
}


