package com.example.just_dance.retrofit.resetPass;

import com.example.just_dance.retrofit.response.ResetPassResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ResetPassInterface {
    @GET("/resetpass")
    Call<ResetPassResponse> getResetPass(@Query("ID") int ID);
}
