package com.example.just_dance.retrofit.misClases;

import com.example.just_dance.retrofit.response.MisClasesResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MisClasesInterface {
    @GET("/misclases")
    Call<MisClasesResponse> getMisClases(@Query("ID") int id);
}
