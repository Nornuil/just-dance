package com.example.just_dance.retrofit.login;

import android.content.Context;

import com.example.just_dance.retrofit.ApiClient;
import com.example.just_dance.retrofit.response.LoginResponse;
import com.example.just_dance.utils.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginAPI {
    public void getLogin(String Email, int Password, Context context, final LoginCallback callback) {

        if (Utils.getInstance().isNetworkConnected(context)) {

            LoginInterface apiService = ApiClient.getClient().create(LoginInterface.class);
            Call<LoginResponse> call = apiService.getLogin(Email, Password);
            call.enqueue(new Callback<LoginResponse>() {
                @Override
                public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                    if (response.isSuccessful()) {
                        callback.onSuccess(response.body());
                    } else {
                        callback.onError();
                    }
                }

                @Override
                public void onFailure(Call<LoginResponse> call, Throwable t) {
                    callback.onError();
                }
            });

        } else {
            callback.onNetworkError();
        }

    }
}
