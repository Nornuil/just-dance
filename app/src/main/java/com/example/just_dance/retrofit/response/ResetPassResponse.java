package com.example.just_dance.retrofit.response;

import com.google.gson.annotations.SerializedName;

public class ResetPassResponse {

    @SerializedName("actualizar")
    private int actualizar;

    public ResetPassResponse(int actualizar) {
        this.actualizar = actualizar;
    }

    public int getActualizar() {
        return actualizar;
    }

    public void setActualizar(int actualizar) {
        this.actualizar = actualizar;
    }
}
