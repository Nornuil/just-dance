package com.example.just_dance.retrofit.registro;

import com.example.just_dance.retrofit.response.RegistroResponse;

import java.util.Date;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;


public interface RegistroInterface {
    @POST("/addalumno")
    @FormUrlEncoded
    Call<RegistroResponse> postAddAlumno(@Field("Password") int Password,
                                         @Field("Nombre") String Nombre,
                                         @Field("Apellido") String Apellido,
                                         @Field("Email") String Email,
                                         @Field("Fecha_Nacimiento") Date Fecha);
}

