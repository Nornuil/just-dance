package com.example.just_dance.retrofit.actualizarPass;

import com.example.just_dance.retrofit.response.ActualizarPassResponse;


public interface ActualizarPassCallback {
    void onSuccess(ActualizarPassResponse response);

    void onError();

    void onNetworkError();
}
