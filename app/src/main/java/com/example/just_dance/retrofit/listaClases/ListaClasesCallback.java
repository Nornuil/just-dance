package com.example.just_dance.retrofit.listaClases;


import com.example.just_dance.retrofit.response.ListaClasesResponse;

public interface ListaClasesCallback {
    void onSuccess(ListaClasesResponse response);

    void onError();

    void onNetworkError();
}
