package com.example.just_dance.retrofit.actualizarEstado;

import com.example.just_dance.retrofit.response.ActualizarEstadoResponse;

import retrofit2.Call;
import retrofit2.http.PUT;
import retrofit2.http.Query;

public interface ActualizarEstadoInterface {
    @PUT("/actualizaestado")
    Call<ActualizarEstadoResponse> putEstado(@Query("ID") int id,
                                             @Query("Estado") int estado);

}
