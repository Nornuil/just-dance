package com.example.just_dance.retrofit.login;

import com.example.just_dance.retrofit.response.LoginResponse;

public interface LoginCallback {
    void onSuccess(LoginResponse response);

    void onError();

    void onNetworkError();

}
