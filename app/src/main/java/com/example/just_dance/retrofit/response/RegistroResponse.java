package com.example.just_dance.retrofit.response;

import com.google.gson.annotations.SerializedName;

public class RegistroResponse {

    @SerializedName("registro")
    private int registro;

    public RegistroResponse(int registro) {
        this.registro = registro;

    }

    public int getRegistro() {
        return registro;
    }

    public void setRegistro(int registro) {
        this.registro = registro;
    }
}
