package com.example.just_dance.retrofit.inscripcion;

import com.example.just_dance.retrofit.response.InscripcionResponse;


public interface InscripcionCallback {
    void onSuccess(InscripcionResponse response);

    void onError();

    void onNetworkError();
}
