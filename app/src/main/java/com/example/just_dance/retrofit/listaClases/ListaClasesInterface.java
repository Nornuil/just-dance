package com.example.just_dance.retrofit.listaClases;

import com.example.just_dance.retrofit.response.ListaClasesResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ListaClasesInterface {
    @GET("/listaclases")
    Call<ListaClasesResponse> getListaClases(@Query("ID") int id);
}
