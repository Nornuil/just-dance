package com.example.just_dance.retrofit.listaAlumnos;

import com.example.just_dance.retrofit.response.ListaAlumnosResponse;

public interface ListaAlumnosCallback {
    void onSuccess(ListaAlumnosResponse response);

    void onError();

}
