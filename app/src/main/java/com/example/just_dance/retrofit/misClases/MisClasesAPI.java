package com.example.just_dance.retrofit.misClases;

import android.content.Context;

import com.example.just_dance.retrofit.ApiClient;
import com.example.just_dance.retrofit.response.MisClasesResponse;
import com.example.just_dance.utils.SharedPreferencesManager;
import com.example.just_dance.utils.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MisClasesAPI {

    public void getMisClases(Context context, MisClasesCallback callback) {
        if (Utils.getInstance().isNetworkConnected(context)) {

            MisClasesInterface apiService = ApiClient.getClient().create(MisClasesInterface.class);
            Call<MisClasesResponse> call = apiService.getMisClases(SharedPreferencesManager.getInstance(context).getInt("id"));
            call.enqueue(new Callback<MisClasesResponse>() {
                @Override
                public void onResponse(Call<MisClasesResponse> call, Response<MisClasesResponse> response) {
                    if (response.isSuccessful()) {
                        callback.onSuccess(response.body());
                    } else {
                        callback.onError();
                    }
                }

                @Override
                public void onFailure(Call<MisClasesResponse> call, Throwable t) {
                    callback.onError();
                }
            });

        } else {
            callback.onNetworkError();
        }
    }

}
