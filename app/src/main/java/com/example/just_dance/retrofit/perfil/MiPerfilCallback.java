package com.example.just_dance.retrofit.perfil;


import com.example.just_dance.retrofit.response.MiPefilResponse;

public interface MiPerfilCallback {
    void onSuccess(MiPefilResponse response);

    void onError();

    void onNetworkError();
}
