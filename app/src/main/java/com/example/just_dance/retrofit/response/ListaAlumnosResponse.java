package com.example.just_dance.retrofit.response;


import com.example.just_dance.model.ListaAlumnosModel;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ListaAlumnosResponse {
    @SerializedName("usuarios")
    private List<ListaAlumnosModel> listaUsuarios;

    public ListaAlumnosResponse(List<ListaAlumnosModel> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }

    public ArrayList<ListaAlumnosModel> getListaUsuarios() {
        return (ArrayList<ListaAlumnosModel>) listaUsuarios;
    }

    public void setlistaUsuarios(List<ListaAlumnosModel> listaUsuarios) {
        this.listaUsuarios = listaUsuarios;
    }
}
