package com.example.just_dance.retrofit.response;

import com.example.just_dance.model.LoginModel;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LoginResponse {


    @SerializedName("data")
    private List<LoginModel> data;

    public LoginResponse(List<LoginModel> data) {
        this.data = data;
    }

    public List<LoginModel> getData() {
        return data;
    }

    public void setData(List<LoginModel> data) {
        this.data = data;
    }

}
