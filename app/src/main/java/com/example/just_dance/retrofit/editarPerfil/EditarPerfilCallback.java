package com.example.just_dance.retrofit.editarPerfil;

import com.example.just_dance.retrofit.response.EditarPerfilResponse;


public interface EditarPerfilCallback {
    void onSuccess(EditarPerfilResponse response);

    void onError();

    void onNetworkError();

    void onEmailError();
}
