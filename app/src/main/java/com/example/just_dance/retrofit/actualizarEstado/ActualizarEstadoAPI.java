package com.example.just_dance.retrofit.actualizarEstado;

import android.content.Context;

import com.example.just_dance.retrofit.ApiClient;
import com.example.just_dance.retrofit.response.ActualizarEstadoResponse;
import com.example.just_dance.utils.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActualizarEstadoAPI {
    public void putEstado(int id, int estado, Context context, final ActualizarEstadoCallback callback) {
        if (Utils.getInstance().isNetworkConnected(context)) {
            ActualizarEstadoInterface apiService = ApiClient.getClient().create(ActualizarEstadoInterface.class);
            Call<ActualizarEstadoResponse> call = apiService.putEstado(id, estado);
            call.enqueue(new Callback<ActualizarEstadoResponse>() {
                @Override
                public void onResponse(Call<ActualizarEstadoResponse> call, Response<ActualizarEstadoResponse> response) {
                    if (response.isSuccessful()) {
                        callback.onSuccess(response.body());
                    } else {
                        callback.onError();
                    }
                }

                @Override
                public void onFailure(Call<ActualizarEstadoResponse> call, Throwable t) {
                    callback.onError();
                }
            });

        } else {
            callback.onNetworkError();
        }
    }
}
