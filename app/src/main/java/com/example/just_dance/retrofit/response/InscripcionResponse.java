package com.example.just_dance.retrofit.response;

import com.google.gson.annotations.SerializedName;

public class InscripcionResponse {

    @SerializedName("inscripcion")
    private int inscripcion;

    public InscripcionResponse(int inscripcion) {
        this.inscripcion = inscripcion;
    }

    public int getInscripcion() {
        return inscripcion;
    }

    public void setInscripcion(int inscripcion) {
        this.inscripcion = inscripcion;
    }
}
