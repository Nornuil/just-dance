package com.example.just_dance.retrofit.actualizarPass;

import android.content.Context;

import com.example.just_dance.retrofit.ApiClient;
import com.example.just_dance.retrofit.response.ActualizarPassResponse;
import com.example.just_dance.utils.SharedPreferencesManager;
import com.example.just_dance.utils.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActualizarPassAPI {

    public void getActualizarPass(Context context, int Password, final ActualizarPassCallback callback) {
        if (Utils.getInstance().isNetworkConnected(context)) {
            ActualizarPassInterface apiService = ApiClient.getClient().create(ActualizarPassInterface.class);
            Call<ActualizarPassResponse> call = apiService.getActualizarPass(SharedPreferencesManager.getInstance(context).getInt("id"), Password);
            call.enqueue(new Callback<ActualizarPassResponse>() {
                @Override
                public void onResponse(Call<ActualizarPassResponse> call, Response<ActualizarPassResponse> response) {
                    if (response.isSuccessful()) {
                        callback.onSuccess(response.body());
                    } else {
                        callback.onError();
                    }
                }

                @Override
                public void onFailure(Call<ActualizarPassResponse> call, Throwable t) {
                    callback.onError();
                }
            });
        } else {
            callback.onNetworkError();
        }
    }

}
