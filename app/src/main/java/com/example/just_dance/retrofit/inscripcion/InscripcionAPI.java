package com.example.just_dance.retrofit.inscripcion;

import android.content.Context;

import com.example.just_dance.retrofit.ApiClient;
import com.example.just_dance.retrofit.response.InscripcionResponse;
import com.example.just_dance.utils.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InscripcionAPI {

    public void getInscripcion(int ID, int ClaseID, Context context, final InscripcionCallback callback) {
        if (Utils.getInstance().isNetworkConnected(context)) {
            InscripcionInterface apiService = ApiClient.getClient().create(InscripcionInterface.class);
            Call<InscripcionResponse> call = apiService.getInscripcion(ID, ClaseID);
            call.enqueue(new Callback<InscripcionResponse>() {
                @Override
                public void onResponse(Call<InscripcionResponse> call, Response<InscripcionResponse> response) {
                    if (response.isSuccessful() && response.body().getInscripcion() != 0) {
                        callback.onSuccess(response.body());
                    } else {
                        callback.onError();
                    }
                }

                @Override
                public void onFailure(Call<InscripcionResponse> call, Throwable t) {
                    callback.onError();
                }
            });
        } else {
            callback.onNetworkError();
        }
    }

}
