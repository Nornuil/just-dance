package com.example.just_dance.retrofit.perfil;

import com.example.just_dance.retrofit.response.MiPefilResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MiPerfilInterface {

    @GET("/miperfil")
    Call<MiPefilResponse> getMiPerfil(@Query("ID") int id);

}
