package com.example.just_dance.retrofit.response;

import com.example.just_dance.model.ListaClasesModel;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ListaClasesResponse {
    @SerializedName("clases")
    private List<ListaClasesModel> listaClases;

    public ListaClasesResponse(List<ListaClasesModel> listaClases) {
        this.listaClases = listaClases;
    }

    public ArrayList<ListaClasesModel> getListaClases() {
        return (ArrayList<ListaClasesModel>) listaClases;
    }

    public void setListaClases(List<ListaClasesModel> listaClases) {
        this.listaClases = listaClases;
    }
}
