package com.example.just_dance.retrofit.actualizarEstado;

import com.example.just_dance.retrofit.response.ActualizarEstadoResponse;


public interface ActualizarEstadoCallback {
    void onSuccess(ActualizarEstadoResponse response);

    void onError();

    void onNetworkError();
}
