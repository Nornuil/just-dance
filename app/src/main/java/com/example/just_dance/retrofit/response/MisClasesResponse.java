package com.example.just_dance.retrofit.response;


import com.example.just_dance.model.MisClasesModel;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class MisClasesResponse {
    @SerializedName("clases")
    private List<MisClasesModel> misClases;

    public MisClasesResponse(List<MisClasesModel> misClases) {
        this.misClases = misClases;
    }

    public ArrayList<MisClasesModel> getMisClases() {
        return (ArrayList<MisClasesModel>) misClases;
    }

    public void setMisClases(List<MisClasesModel> misClases) {
        this.misClases = misClases;
    }
}
