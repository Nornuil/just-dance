package com.example.just_dance.retrofit.login;

import com.example.just_dance.retrofit.response.LoginResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface LoginInterface {
    @GET("/login")
    Call<LoginResponse> getLogin(@Query("Email") String Mail,
                                 @Query("Password") int Password);
}

