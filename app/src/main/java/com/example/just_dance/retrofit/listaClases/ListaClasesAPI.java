package com.example.just_dance.retrofit.listaClases;


import android.content.Context;

import com.example.just_dance.retrofit.ApiClient;
import com.example.just_dance.retrofit.response.ListaClasesResponse;
import com.example.just_dance.utils.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListaClasesAPI {

    public void getListaClases(int ID, Context context, ListaClasesCallback callback) {

        if (Utils.getInstance().isNetworkConnected(context)) {
            ListaClasesInterface apiService = ApiClient.getClient().create(ListaClasesInterface.class);
            Call<ListaClasesResponse> call = apiService.getListaClases(ID);
            call.enqueue(new Callback<ListaClasesResponse>() {
                @Override
                public void onResponse(Call<ListaClasesResponse> call, Response<ListaClasesResponse> response) {
                    if (response.isSuccessful()) {
                        callback.onSuccess(response.body());
                    } else {
                        callback.onError();
                    }
                }

                @Override
                public void onFailure(Call<ListaClasesResponse> call, Throwable t) {
                    callback.onError();
                }
            });
        } else {
            callback.onNetworkError();
        }
    }
}
