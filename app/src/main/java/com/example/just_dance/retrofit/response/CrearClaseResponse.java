package com.example.just_dance.retrofit.response;

import com.google.gson.annotations.SerializedName;

public class CrearClaseResponse {

    @SerializedName("crear")
    private int crear;

    public CrearClaseResponse(int crear) {
        this.crear = crear;
    }

    public int getCrear() {
        return crear;
    }

    public void setCrear(int crear) {
        this.crear = crear;
    }
}
