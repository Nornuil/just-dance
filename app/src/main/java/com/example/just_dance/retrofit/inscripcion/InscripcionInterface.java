package com.example.just_dance.retrofit.inscripcion;

import com.example.just_dance.retrofit.response.InscripcionResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface InscripcionInterface {
    @GET("/inscripcion")
    Call<InscripcionResponse> getInscripcion(@Query("ID") int ID,
                                             @Query("ClaseID") int ClaseID);
}
