package com.example.just_dance.retrofit.editarPerfil;

import android.content.Context;

import com.example.just_dance.retrofit.ApiClient;
import com.example.just_dance.retrofit.response.EditarPerfilResponse;
import com.example.just_dance.utils.Utils;

import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditarPerfilAPI {

    public void getEditarPerfil(String nombre, String apellido, String email, Date nacimiento, int id, Context context, EditarPerfilCallback callback) {
        if (Utils.getInstance().isNetworkConnected(context)) {
            EditarPerfilInterface apiService = ApiClient.getClient().create(EditarPerfilInterface.class);
            Call<EditarPerfilResponse> call = apiService.getEditarPerfil(nombre, apellido, email, nacimiento, id);
            call.enqueue(new Callback<EditarPerfilResponse>() {
                @Override
                public void onResponse(Call<EditarPerfilResponse> call, Response<EditarPerfilResponse> response) {
                    if (response.isSuccessful()) {

                        switch (response.body().getActualizar()) {

                            case 0:
                                callback.onEmailError();
                                break;
                            case 1:
                                callback.onSuccess(response.body());
                                break;
                            default:
                                callback.onError();
                        }


                    } else {
                        callback.onError();
                    }
                }

                @Override
                public void onFailure(Call<EditarPerfilResponse> call, Throwable t) {
                    callback.onError();
                }
            });

        } else {
            callback.onNetworkError();
        }


    }
}
