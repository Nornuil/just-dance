package com.example.just_dance.retrofit.resetPass;

import android.content.Context;

import com.example.just_dance.retrofit.ApiClient;
import com.example.just_dance.retrofit.response.ResetPassResponse;
import com.example.just_dance.utils.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPassAPI {

    public void getResetPass(int ID, Context context, final ResetPassCallback callback) {
        if (Utils.getInstance().isNetworkConnected(context)) {
            ResetPassInterface apiService = ApiClient.getClient().create(ResetPassInterface.class);
            Call<ResetPassResponse> call = apiService.getResetPass(ID);
            call.enqueue(new Callback<ResetPassResponse>() {
                @Override
                public void onResponse(Call<ResetPassResponse> call, Response<ResetPassResponse> response) {
                    if (response.isSuccessful()) {
                        callback.onSuccess(response.body());
                    } else {
                        callback.onError();
                    }
                }

                @Override
                public void onFailure(Call<ResetPassResponse> call, Throwable t) {
                    callback.onError();
                }
            });
        } else {
            callback.onNetworkError();
        }
    }

}
