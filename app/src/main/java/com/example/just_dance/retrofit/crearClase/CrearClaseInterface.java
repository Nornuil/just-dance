package com.example.just_dance.retrofit.crearClase;

import com.example.just_dance.retrofit.response.CrearClaseResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface CrearClaseInterface {
    @GET("/crearclase")
    Call<CrearClaseResponse> getAddClase(@Query("Nombre") String Nombre,
                                         @Query("Aula") int Aula,
                                         @Query("Dia") int Dia,
                                         @Query("Horario_Inicio") String HorarioInicio,
                                         @Query("Horario_Fin") String HorarioFin,
                                         @Query("Vacantes") int Vacantes);
}
