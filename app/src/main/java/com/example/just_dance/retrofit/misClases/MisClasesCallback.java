package com.example.just_dance.retrofit.misClases;


import com.example.just_dance.retrofit.response.MisClasesResponse;

public interface MisClasesCallback {
    void onSuccess(MisClasesResponse response);

    void onError();

    void onNetworkError();
}
