package com.example.just_dance.retrofit.listaAlumnos;

import com.example.just_dance.retrofit.response.ListaAlumnosResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ListaAlumnosInterface {
    @GET("/listaalumno")
    Call<ListaAlumnosResponse> getListaAlumnos();

//dentro del Call es lo q me devuelvo, las Query son lo q manda como parametro como postman
}
