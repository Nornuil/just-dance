package com.example.just_dance.retrofit.resetPass;


import com.example.just_dance.retrofit.response.ResetPassResponse;

public interface ResetPassCallback {
    void onSuccess(ResetPassResponse response);

    void onError();

    void onNetworkError();
}
