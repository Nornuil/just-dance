package com.example.just_dance.retrofit.response;

import com.google.gson.annotations.SerializedName;

public class ActualizarPassResponse {
    @SerializedName("actualizar")
    private int actualizar;

    public ActualizarPassResponse(int actualizar) {
        this.actualizar = actualizar;
    }

    public int getActualizar() {
        return actualizar;
    }

    public void setActualizar(int actualizar) {
        this.actualizar = actualizar;
    }
}
