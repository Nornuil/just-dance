package com.example.just_dance.model;

import android.content.Intent;

public class MenuModel {
    private int imageResource;
    private String titleResource;
    private Intent destination;
    private boolean finishActivity;

    public MenuModel(int imageResource, String titleResource, Intent destination, boolean finishActivity) {
        this.imageResource = imageResource;
        this.titleResource = titleResource;
        this.destination = destination;
        this.finishActivity = finishActivity;
    }

    public int getImageResource() {
        return imageResource;
    }

    public void setImageResource(int imageResource) {
        this.imageResource = imageResource;
    }

    public String getTitleResource() {
        return titleResource;
    }

    public void setTitleResource(String titleResource) {
        this.titleResource = titleResource;
    }

    public Intent getDestination() {
        return destination;
    }

    public void setDestination(Intent destination) {
        this.destination = destination;
    }

    public boolean isFinishActivity() {
        return finishActivity;
    }

    public void setFinishActivity(boolean finishActivity) {
        this.finishActivity = finishActivity;
    }
}
