package com.example.just_dance.model;

import com.google.gson.annotations.SerializedName;

public class LoginModel {
    @SerializedName("ID")
    private int id;
    @SerializedName("Estado")
    private int estado;

    public LoginModel(int id, int estado) {
        this.id = id;
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }
}
