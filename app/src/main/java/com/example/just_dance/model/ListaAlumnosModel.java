package com.example.just_dance.model;

import com.google.gson.annotations.SerializedName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ListaAlumnosModel {
    @SerializedName("Nombre")
    private String nombre;
    @SerializedName("Apellido")
    private String apellido;
    @SerializedName("Email")
    private String email;
    @SerializedName("Estado")
    private int estado;
    @SerializedName("ID")
    private int id;
    @SerializedName("Fecha_Nacimiento")
    private Date fecha;

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public String getEstadoTitle() {
        switch (estado) {
            case 0:
                return "Inactivo";

            case 1:
                return "Alumno";

            case 2:
                return "Administrativo";

            case 3:
                return "Director";

            default:
                return "";
        }
    }

    public String getFechaStr() {
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd", new Locale("es", "ES"));
        return sdf.format(fecha);
    }

    public void setFechaStr(String fechaStr) {
        Date nuevaFecha;
        try {
            SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd", new Locale("es", "ES"));
            nuevaFecha = sdf.parse(fechaStr);
        } catch (ParseException ex) {
            nuevaFecha = new Date();
        }
        fecha = nuevaFecha;
    }

    public ListaAlumnosModel(String nombre, String apellido, String email, int estado, int id, Date fecha) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.estado = estado;
        this.id = id;
        this.fecha = fecha;
    }
}


