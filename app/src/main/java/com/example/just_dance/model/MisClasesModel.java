package com.example.just_dance.model;

import com.google.gson.annotations.SerializedName;

public class MisClasesModel {

    @SerializedName("Nombre")
    private String Nombre;
    @SerializedName("Aula")
    private int Aula;
    @SerializedName("Dia")
    private int Dia;
    @SerializedName("Horario_Inicio")
    private String HorarioInicio;
    @SerializedName("Horario_Fin")
    private String HorarioFin;

    public String getHorarioInicio() {
        return HorarioInicio;
    }

    public void setHorarioInicio(String horarioInicio) {
        HorarioInicio = horarioInicio;
    }

    public String getHorarioFin() {
        return HorarioFin;
    }

    public void setHorarioFin(String horarioFin) {
        HorarioFin = horarioFin;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public int getAula() {
        return Aula;
    }

    public void setAula(int aula) {
        Aula = aula;
    }

    public int getDia() {
        return Dia;
    }

    public void setDia(int dia) {
        Dia = dia;
    }


    public MisClasesModel(String nombre, int aula, int dia, String horarioInicio, String horarioFin) {
        this.Nombre = nombre;
        this.Aula = aula;
        this.Dia = dia;
        this.HorarioInicio = horarioInicio;
        this.HorarioFin = horarioFin;
    }
}
