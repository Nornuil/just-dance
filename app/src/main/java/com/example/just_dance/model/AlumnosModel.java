package com.example.just_dance.model;

import com.google.gson.annotations.SerializedName;

public class AlumnosModel {
    @SerializedName("UsuarioID")
    private int usuarioId;
    @SerializedName("Password")
    private int password;

    public AlumnosModel(int usuarioId, int password) {
        this.usuarioId = usuarioId;
        this.password = password;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public int getPassword() {
        return password;
    }

    public void setPassword(int password) {
        this.password = password;
    }
}
