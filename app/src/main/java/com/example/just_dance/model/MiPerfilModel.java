package com.example.just_dance.model;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MiPerfilModel {
    @SerializedName("ID")
    private int id;
    @SerializedName("Password")
    private int password;
    @SerializedName("Nombre")
    private String nombre;
    @SerializedName("Apellido")
    private String apellido;
    @SerializedName("Email")
    private String email;
    @SerializedName("Estado")
    private int estado;
    @SerializedName("Fecha_Nacimiento")
    private Date fecha_nacimiento;

    public MiPerfilModel(int id, int password, String nombre, String apellido, String email, int estado, Date fecha_nacimiento) {
        this.id = id;
        this.password = password;
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
        this.estado = estado;
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPassword() {
        return password;
    }

    public void setPassword(int password) {
        this.password = password;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public Date getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public String getFechaStr() {
        SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd", new Locale("es", "ES"));
        return sdf.format(fecha_nacimiento);
    }

    public void setFecha_nacimiento(Date fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }
}
