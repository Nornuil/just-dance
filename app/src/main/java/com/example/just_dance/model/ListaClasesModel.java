package com.example.just_dance.model;

import com.google.gson.annotations.SerializedName;

public class ListaClasesModel {
    @SerializedName("Nombre")
    private String Nombre;
    @SerializedName("Aula")
    private int Aula;
    @SerializedName("Dia")
    private int Dia;
    @SerializedName("Horario_Inicio")
    private String HorarioInicio;
    @SerializedName("Horario_Fin")
    private String HorarioFin;
    @SerializedName("Vacantes")
    private int Vacantes;
    ///////////////
    @SerializedName("ClaseID")
    private int ClaseID;

    public String getHorarioFin() {
        return HorarioFin;
    }

    public void setHorarioFin(String horarioFin) {
        HorarioFin = horarioFin;
    }

    public int getClaseID() {
        return ClaseID;
    }

    public void setClaseID(int claseID) {
        ClaseID = claseID;
    }

    ///////////////////
    public int getVacantes() {
        return Vacantes;
    }

    public void setVacantes(int vacantes) {
        Vacantes = vacantes;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public int getAula() {
        return Aula;
    }

    public void setAula(int aula) {
        Aula = aula;
    }

    public int getDia() {
        return Dia;
    }

    public void setDia(int dia) {
        Dia = dia;
    }

    public String getHorarioInicio() {
        return HorarioInicio;
    }

    public void setHorarioInicio(String horario) {
        HorarioInicio = horario;
    }

    public ListaClasesModel(String nombre, int aula, int dia, String horarioInicio, String horarioFin, int vacantes, int claseID) {
        this.Nombre = nombre;
        this.Aula = aula;
        this.Dia = dia;
        this.HorarioInicio = horarioInicio;
        this.HorarioFin = horarioFin;
        this.Vacantes = vacantes;
        this.ClaseID = claseID;
    }
}
