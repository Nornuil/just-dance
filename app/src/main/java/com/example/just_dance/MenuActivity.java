package com.example.just_dance;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.just_dance.adapter.MenuAdapter;
import com.example.just_dance.adapter.MenuAdapterListener;
import com.example.just_dance.model.MenuModel;
import com.example.just_dance.utils.SharedPreferencesManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MenuActivity extends AppCompatActivity {

    // Binding view with butterKnife
    @BindView(R.id.menuData)
    RecyclerView menuList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        ButterKnife.bind(this);

        List<MenuModel> dataList = generateMenuData();

        menuList.setLayoutManager(new GridLayoutManager(this, 2));
        MenuAdapter adapter = new MenuAdapter(dataList, this, new MenuAdapterListener() {
            @Override
            public void onItemClick(Intent intentTransition, boolean finishActivity) {
                startActivity(intentTransition);
                if (finishActivity) {
                    SharedPreferencesManager.getInstance(MenuActivity.this).clear();
                    finish();
                }
            }
        });
        menuList.setAdapter(adapter);
    }

    private List<MenuModel> generateMenuData() {
        List<MenuModel> data = new ArrayList<>();
        int estado = SharedPreferencesManager.getInstance(this).getInt("estado");

        Intent miPerfilIntent = new Intent(MenuActivity.this, MiPerfilActivity.class);
        MenuModel miPerfilItem = new MenuModel(R.drawable.perfil, getString(R.string.tPerfil), miPerfilIntent, false);
        data.add(miPerfilItem);

        if (estado >= 1) {
            Intent misClasesIntent = new Intent(MenuActivity.this, MisClasesActivity.class);
            MenuModel misClasesItem = new MenuModel(R.drawable.registrarse, getString(R.string.tClases), misClasesIntent, false);
            data.add(misClasesItem);
        }

        if (estado >= 2) {
            Intent listaAlumnosIntent = new Intent(MenuActivity.this, ListaAlumnosActivity.class);
            MenuModel listaAlumnosItem = new MenuModel(R.drawable.anotador, getString(R.string.tListaAlumnos), listaAlumnosIntent, false);
            data.add(listaAlumnosItem);
        }

        if (estado >= 1) {
            Intent listaClasesIntent = new Intent(MenuActivity.this, ListaClasesActivity.class);
            MenuModel listaClasesItem = new MenuModel(R.drawable.libro, getString(R.string.tListaClases), listaClasesIntent, false);
            data.add(listaClasesItem);
        }

        if (estado >= 2) {
            Intent crearClaseIntent = new Intent(MenuActivity.this, CrearClaseActivity.class);
            MenuModel crearClaseItem = new MenuModel(R.drawable.crearclase, getString(R.string.tCrearClase), crearClaseIntent, false);
            data.add(crearClaseItem);
        }

        Intent cerrarSesionIntent = new Intent(MenuActivity.this, MainActivity.class);
        MenuModel cerrarSesionItem = new MenuModel(R.drawable.exit, getString(R.string.tLogout), cerrarSesionIntent, true);
        data.add(cerrarSesionItem);

        return data;
    }
}