package com.example.just_dance;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.just_dance.model.MiPerfilModel;
import com.example.just_dance.retrofit.actualizarPass.ActualizarPassAPI;
import com.example.just_dance.retrofit.actualizarPass.ActualizarPassCallback;
import com.example.just_dance.retrofit.perfil.MiPerfilAPI;
import com.example.just_dance.retrofit.perfil.MiPerfilCallback;
import com.example.just_dance.retrofit.response.ActualizarPassResponse;
import com.example.just_dance.retrofit.response.MiPefilResponse;
import com.example.just_dance.utils.CustomProgressDialog;
import com.google.android.material.textfield.TextInputLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MiPerfilActivity extends AppCompatActivity {
    private CustomProgressDialog progressDialog = new CustomProgressDialog();


    //cambiar los data por editText
    @BindView(R.id.tNombreData)
    TextView tNombreData;
    @BindView(R.id.tApellidoData)
    TextView tApellidoData;
    @BindView(R.id.tEmailData)
    TextView tEmailData;
    @BindView(R.id.tNacimientoData)
    TextView tNacimientoData;
    @BindView(R.id.tContraseñaData)
    TextView tContraseñaData;
    @BindView(R.id.tEstadoData)
    TextView tEstadoData;
    @BindView(R.id.bActualizar)
    ImageView bActualizar;
    TextInputLayout cPasswordWrapper;
    EditText cPassword;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_miperfil);
        ButterKnife.bind(this);


        progressDialog.show(this, "Cargando...");
        new MiPerfilAPI().getMiPerfil(this, new MiPerfilCallback() {
            @Override
            public void onSuccess(MiPefilResponse response) {

                MiPerfilModel perfil = response.getPerfil().get(0);

                switch (perfil.getEstado()) {

                    case 0:
                        tEstadoData.setText("Inactivo");
                        break;
                    case 1:
                        tEstadoData.setText("Alumno");
                        break;
                    case 2:
                        tEstadoData.setText("Administrativo");
                        break;
                    case 3:
                        tEstadoData.setText("Director");
                        break;
                }


                tNombreData.setText(perfil.getNombre());
                tApellidoData.setText(perfil.getApellido());
                tEmailData.setText(perfil.getEmail());
                tContraseñaData.setText(String.valueOf(perfil.getPassword()));
                tNacimientoData.setText(perfil.getFechaStr());
                //tEstadoData.setText(String.valueOf(perfil.getEstado()));
                progressDialog.dismiss();
            }

            @Override
            public void onError() {
                progressDialog.dismiss();
                Toast.makeText(MiPerfilActivity.this, "Hubo un error", Toast.LENGTH_LONG).show();
                finish();

            }

            @Override
            public void onNetworkError() {
                Toast.makeText(MiPerfilActivity.this, "El dispositivo no esta conectado a la red", Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
                finish();
            }

        });
        final AlertDialog dialogBuilder = new AlertDialog.Builder(this).create();
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_actualizar_perfil, null);


        /////actualizar contraseña
        bActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cPassword != null) {
                    cPassword.setText("");
                }
                if (cPasswordWrapper != null) {
                    cPasswordWrapper.setErrorEnabled(false);
                    cPasswordWrapper.setError(null);
                }
                dialogBuilder.setView(dialogView);
                dialogBuilder.show();

            }
        });


        cPassword = (EditText) dialogView.findViewById(R.id.cPassword);
        cPasswordWrapper = dialogView.findViewById(R.id.cPasswordWrapper);
        Button bCancelar = (Button) dialogView.findViewById(R.id.bCancelar);
        Button bAceptar = (Button) dialogView.findViewById(R.id.bAceptar);


        bAceptar.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {
                if (isValidForm()) {

                    progressDialog.show(MiPerfilActivity.this, "Cargando...");
                    dialogBuilder.dismiss();
                    new ActualizarPassAPI().getActualizarPass(MiPerfilActivity.this, Integer.parseInt(cPassword.getText().toString()), new ActualizarPassCallback() {
                        @Override
                        public void onSuccess(ActualizarPassResponse response) {
                            Toast.makeText(MiPerfilActivity.this, "Contraseña Actualizada", Toast.LENGTH_LONG).show();
                            progressDialog.dismiss();
                            tContraseñaData.setText(cPassword.getText().toString());
                        }

                        @Override
                        public void onError() {
                            Toast.makeText(MiPerfilActivity.this, "Actualización Rechazada", Toast.LENGTH_LONG).show();
                            progressDialog.dismiss();
                        }

                        @Override
                        public void onNetworkError() {
                            Toast.makeText(MiPerfilActivity.this, "El dispositivo no esta conectado a la red", Toast.LENGTH_LONG).show();
                            progressDialog.dismiss();
                        }
                    });

                }


            }
        });
        bCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // DO SOMETHINGS
                dialogBuilder.dismiss();
            }
        });


    }

    private Boolean isValidForm() {
        boolean validForm = true;

        if (cPassword.getText().toString().length() < 6) {
            cPasswordWrapper.setErrorEnabled(true);
            cPasswordWrapper.setError("El campo debe conterner entre 6 y 8 dígitos");
            validForm = false;
        } else {
            cPasswordWrapper.setErrorEnabled(false);
            cPasswordWrapper.setError(null);
        }

        return validForm;
    }


}