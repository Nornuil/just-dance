package com.example.just_dance;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.just_dance.adapter.ListaClasesAdapter;
import com.example.just_dance.model.ListaClasesModel;
import com.example.just_dance.retrofit.inscripcion.InscripcionAPI;
import com.example.just_dance.retrofit.inscripcion.InscripcionCallback;
import com.example.just_dance.retrofit.listaClases.ListaClasesAPI;
import com.example.just_dance.retrofit.listaClases.ListaClasesCallback;
import com.example.just_dance.retrofit.response.InscripcionResponse;
import com.example.just_dance.retrofit.response.ListaClasesResponse;
import com.example.just_dance.utils.CustomProgressDialog;
import com.example.just_dance.utils.SharedPreferencesManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListaClasesActivity extends AppCompatActivity {

    @BindView(R.id.recyclerClases)
    RecyclerView recyclerClases;
    private CustomProgressDialog progressDialog = new CustomProgressDialog();
    private ListaClasesAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_clases);
        ButterKnife.bind(this);


        recyclerClases.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new ListaClasesAdapter(new ArrayList<>(), this::itemClaseSelect);
        recyclerClases.setAdapter(adapter);
        progressDialog.show(ListaClasesActivity.this, "Cargando...");

        new ListaClasesAPI().getListaClases(SharedPreferencesManager.getInstance(this).getInt("id"), this, new ListaClasesCallback() {
            @Override
            public void onSuccess(ListaClasesResponse response) {
                adapter.updateData(response.getListaClases());

                progressDialog.dismiss();

            }

            @Override
            public void onError() {
                progressDialog.dismiss();
                Toast.makeText(ListaClasesActivity.this, "Hubo un error", Toast.LENGTH_LONG).show();
                finish();
            }

            @Override
            public void onNetworkError() {
                Toast.makeText(ListaClasesActivity.this, "El dispositivo no esta conectado a la red", Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
                finish();
            }
        });
    }


    private void itemClaseSelect(ListaClasesModel clase, int position) {


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //MENSAJE
        builder.setMessage("¿Estas seguro?");
        builder.setTitle("Inscribirse");
        builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                progressDialog.show(ListaClasesActivity.this, "Cargando...");
                //llamar al servicio de la inscripcion
                new InscripcionAPI().getInscripcion(SharedPreferencesManager.getInstance(ListaClasesActivity.this).getInt("id"), clase.getClaseID(), ListaClasesActivity.this, new InscripcionCallback() {//llamar servicio de vacantes
                    @Override
                    public void onSuccess(InscripcionResponse response) {

                        adapter.deleteItemInPosition(position);


                        progressDialog.dismiss();
                    }

                    @Override
                    public void onError() {
                        progressDialog.dismiss();
                        Toast.makeText(ListaClasesActivity.this, "Hubo un error", Toast.LENGTH_LONG).show();
                        // finish();
                    }

                    @Override
                    public void onNetworkError() {
                        Toast.makeText(ListaClasesActivity.this, "El dispositivo no esta conectado a la red", Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }
                });


            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        Log.e("ListaClase", "item: " + position);
        //aca debe ponerse la funcion para confirmar la incripcion
        //Actualizar los valores de alumno
        //adapter.updateItem(alumno,position);
    }


}