package com.example.just_dance.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.just_dance.R;
import com.example.just_dance.model.MenuModel;

import java.util.List;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolderDatos> {

    private List<MenuModel> listaDatos;
    private MenuAdapterListener callback;
    private Context context;

    public MenuAdapter(List<MenuModel> listaDatos, Context context, MenuAdapterListener callback) {
        this.listaDatos = listaDatos;
        this.callback = callback;
        this.context = context;
    }

    @NonNull
    @Override
    public MenuAdapter.ViewHolderDatos onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_item, null, false);
        return new MenuAdapter.ViewHolderDatos(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MenuAdapter.ViewHolderDatos holder, int position) {
        holder.asignarDatos(listaDatos.get(position), position, callback);
    }

    @Override
    public int getItemCount() {
        return listaDatos.size();
    }

    public void updateItem(MenuModel menu, int position) {
        notifyItemChanged(position, menu);
    }

    public class ViewHolderDatos extends RecyclerView.ViewHolder {

        View itemContainer;
        ImageView itemIcon;
        TextView itemTitle;

        public ViewHolderDatos(@NonNull View itemView) {
            super(itemView);
            itemContainer = itemView.findViewById(R.id.itemContainer);
            itemIcon = itemView.findViewById(R.id.itemIcon);
            itemTitle = itemView.findViewById(R.id.itemTitle);
        }

        public void asignarDatos(MenuModel menuOption, int position, MenuAdapterListener callback) {
            itemIcon.setImageResource(menuOption.getImageResource());
            itemTitle.setText(menuOption.getTitleResource());
            itemContainer.setOnClickListener(view -> {
                callback.onItemClick(menuOption.getDestination(), menuOption.isFinishActivity());
            });
        }
    }
}
