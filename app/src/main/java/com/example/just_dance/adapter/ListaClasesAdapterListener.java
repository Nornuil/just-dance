package com.example.just_dance.adapter;


import com.example.just_dance.model.ListaClasesModel;

public interface ListaClasesAdapterListener {
    void itemClick(ListaClasesModel clase, int position);
}
