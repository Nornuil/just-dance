package com.example.just_dance.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.just_dance.R;
import com.example.just_dance.model.ListaClasesModel;

import java.util.ArrayList;
import java.util.List;

public class ListaClasesAdapter extends RecyclerView.Adapter<ListaClasesAdapter.ViewHolderDatos> {

    private List<ListaClasesModel> listaDatos;
    private ListaClasesAdapterListener callback;

    public ListaClasesAdapter(List<ListaClasesModel> listaDatos, ListaClasesAdapterListener callback) {
        this.listaDatos = listaDatos;
        this.callback = callback;
    }

    @NonNull
    @Override
    public ViewHolderDatos onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lista_clases_item, null, false);
        return new ViewHolderDatos(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderDatos holder, int position) {
        holder.asignarDatos(listaDatos.get(position), position, callback);
    }

    @Override
    public int getItemCount() {
        return listaDatos.size();
    }

    public void deleteItemInPosition(int position) {
        listaDatos.remove(position);
        notifyItemRemoved(position);
    }

    public void updateData(ArrayList<ListaClasesModel> newData) {
        listaDatos.clear();
        listaDatos.addAll(newData);
        notifyDataSetChanged();
    }

    public void updateItem(ListaClasesModel clase, int position) {
        notifyItemChanged(position, clase);
    }

    public class ViewHolderDatos extends RecyclerView.ViewHolder {

        TextView tNombreClaseCampo;
        TextView tAulaClaseCampo;
        TextView tDiaClaseCampo;
        TextView tHorarioInicioClaseCampo;
        TextView tHorarioFinClaseCampo;
        TextView tVacantesClaseCampo;
        ImageView tInscribirse;

        public ViewHolderDatos(@NonNull View itemView) {
            super(itemView);
            tNombreClaseCampo = itemView.findViewById(R.id.tNombreClaseCampo);
            tAulaClaseCampo = itemView.findViewById(R.id.tAulaClaseCampo);
            tDiaClaseCampo = itemView.findViewById(R.id.tDiaClaseCampo);
            tHorarioInicioClaseCampo = itemView.findViewById(R.id.tHorarioInicioClaseCampo);
            tHorarioFinClaseCampo = itemView.findViewById(R.id.tHorarioFinClaseCampo);
            tVacantesClaseCampo = itemView.findViewById(R.id.tVacantesClaseCampo);
            tInscribirse = itemView.findViewById(R.id.tInscribirse);
        }

        public void asignarDatos(ListaClasesModel clase, int position, ListaClasesAdapterListener callback) {
            tNombreClaseCampo.setText(clase.getNombre());
            tAulaClaseCampo.setText(String.valueOf(clase.getAula()));
            //tDiaClaseCampo.setText(String.valueOf(clase.getDia()));
            tHorarioInicioClaseCampo.setText(clase.getHorarioInicio());
            tHorarioFinClaseCampo.setText(clase.getHorarioFin());
            tVacantesClaseCampo.setText(String.valueOf(clase.getVacantes()));

            switch (clase.getDia()) {
                case 0:
                    tDiaClaseCampo.setText("Lunes");
                    break;
                case 1:
                    tDiaClaseCampo.setText("Martes");
                    break;
                case 2:
                    tDiaClaseCampo.setText("Miércoles");
                    break;
                case 3:
                    tDiaClaseCampo.setText("Jueves");
                    break;
                case 4:
                    tDiaClaseCampo.setText("Viernes");
                    break;
                case 5:
                    tDiaClaseCampo.setText("Sábado");
                    break;
                case 6:
                    tDiaClaseCampo.setText("Domingo");
                    break;
                default:
                    tDiaClaseCampo.setText("Error");
                    break;
            }

            tInscribirse.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callback.itemClick(clase, position);
                }
            });

        }


    }


}
