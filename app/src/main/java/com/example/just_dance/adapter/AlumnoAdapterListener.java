package com.example.just_dance.adapter;

import com.example.just_dance.model.ListaAlumnosModel;

public interface AlumnoAdapterListener {
    void editarEstado(ListaAlumnosModel alumno, int position);

    void cambioPerfil(ListaAlumnosModel alumno, int position);

    void resetPass(ListaAlumnosModel alumno, int position);
}
