package com.example.just_dance.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.just_dance.R;
import com.example.just_dance.model.MisClasesModel;

import java.util.ArrayList;
import java.util.List;

public class MisClasesAdapter extends RecyclerView.Adapter<MisClasesAdapter.ViewHolderDatos> {

    private List<MisClasesModel> listaDatos;
    private MisClasesAdapterListener callback;

    public MisClasesAdapter(List<MisClasesModel> listaDatos, MisClasesAdapterListener callback) {
        this.listaDatos = listaDatos;
        this.callback = callback;
    }

    @NonNull
    @Override
    public MisClasesAdapter.ViewHolderDatos onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.lista_misclases_item, null, false);
        return new MisClasesAdapter.ViewHolderDatos(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MisClasesAdapter.ViewHolderDatos holder, int position) {
        holder.asignarDatos(listaDatos.get(position), position, callback);
    }

    @Override
    public int getItemCount() {
        return listaDatos.size();
    }

    public void updateData(ArrayList<MisClasesModel> newData) {
        listaDatos.clear();
        listaDatos.addAll(newData);
        notifyDataSetChanged();
    }

    public void updateItem(MisClasesModel clase, int position) {
        notifyItemChanged(position, clase);
    }

    public class ViewHolderDatos extends RecyclerView.ViewHolder {

        TextView tNombreClaseCampo;
        TextView tAulaClaseCampo;
        TextView tDiaClaseCampo;
        TextView tHorarioInicioClaseCampo;
        TextView tHorarioFinClaseCampo;
        //TextView tVacantesClaseCampo;
        //ImageView tInscribirse;

        public ViewHolderDatos(@NonNull View itemView) {
            super(itemView);
            tNombreClaseCampo = itemView.findViewById(R.id.tNombreClaseCampo);
            tAulaClaseCampo = itemView.findViewById(R.id.tAulaClaseCampo);
            tDiaClaseCampo = itemView.findViewById(R.id.tDiaClaseCampo);
            tHorarioInicioClaseCampo = itemView.findViewById(R.id.tHorarioInicioClaseCampo);
            tHorarioFinClaseCampo = itemView.findViewById(R.id.tHorarioFinClaseCampo);

        }

        public void asignarDatos(MisClasesModel clase, int position, MisClasesAdapterListener callback) {
            tNombreClaseCampo.setText(clase.getNombre());
            tAulaClaseCampo.setText(String.valueOf(clase.getAula()));
            tHorarioInicioClaseCampo.setText(clase.getHorarioInicio());
            tHorarioFinClaseCampo.setText(clase.getHorarioFin());


            switch (clase.getDia()) {
                case 0:
                    tDiaClaseCampo.setText("Lunes");
                    break;
                case 1:
                    tDiaClaseCampo.setText("Martes");
                    break;
                case 2:
                    tDiaClaseCampo.setText("Miércoles");
                    break;
                case 3:
                    tDiaClaseCampo.setText("Jueves");
                    break;
                case 4:
                    tDiaClaseCampo.setText("Viernes");
                    break;
                case 5:
                    tDiaClaseCampo.setText("Sábado");
                    break;
                case 6:
                    tDiaClaseCampo.setText("Domingo");
                    break;
                default:
                    tDiaClaseCampo.setText("Error");
                    break;
            }


        }


    }

}
