package com.example.just_dance.adapter;

import android.content.Intent;

public interface MenuAdapterListener {
    void onItemClick(Intent intentTransition, boolean finishActivity);
}
