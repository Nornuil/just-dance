package com.example.just_dance.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.just_dance.R;
import com.example.just_dance.model.ListaAlumnosModel;
import com.example.just_dance.utils.SharedPreferencesManager;

import java.util.ArrayList;
import java.util.List;

public class AlumnosAdapter extends RecyclerView.Adapter<AlumnosAdapter.ViewHolderDatos> {

    private List<ListaAlumnosModel> listaDatos;
    private AlumnoAdapterListener callback;
    private Context context;

    public AlumnosAdapter(List<ListaAlumnosModel> listaDatos, Context context, AlumnoAdapterListener callback) {
        this.listaDatos = listaDatos;
        this.callback = callback;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolderDatos onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list, null, false);
        return new ViewHolderDatos(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderDatos holder, int position) {
        holder.asignarDatos(listaDatos.get(position), position, callback);
    }

    @Override
    public int getItemCount() {
        return listaDatos.size();
    }

    public void updateData(ArrayList<ListaAlumnosModel> newData) {
        listaDatos.clear();
        listaDatos.addAll(newData);
        notifyDataSetChanged();
    }

    public void updateItem(ListaAlumnosModel alumno, int position) {
        notifyItemChanged(position, alumno);
    }

    public class ViewHolderDatos extends RecyclerView.ViewHolder {

        TextView primeritem;
        TextView segundoitem;
        TextView terceritem;
        TextView cuartoitem;
        TextView quintoitem;
        ImageView tCambioEstado;
        View bEditarPefil;
        View bResetPass;

        public ViewHolderDatos(@NonNull View itemView) {
            super(itemView);
            primeritem = itemView.findViewById(R.id.primeritem);
            segundoitem = itemView.findViewById(R.id.segundoitem);
            terceritem = itemView.findViewById(R.id.terceritem);
            cuartoitem = itemView.findViewById(R.id.cuartoitem);
            quintoitem = itemView.findViewById(R.id.quintoitem);
            tCambioEstado = itemView.findViewById(R.id.tCambioEstado);
            bEditarPefil = itemView.findViewById(R.id.bEditarPerfil);
            bResetPass = itemView.findViewById(R.id.bReset);


        }


        public void asignarDatos(ListaAlumnosModel alumno, int position, AlumnoAdapterListener callback) {
            primeritem.setText(alumno.getNombre());
            segundoitem.setText(alumno.getApellido());
            terceritem.setText(alumno.getEmail());
            cuartoitem.setText(alumno.getEstadoTitle());
            quintoitem.setText(alumno.getFechaStr());
            if ((alumno.getEstado() == 2 || alumno.getEstado() == 3) && SharedPreferencesManager.getInstance(context).getInt("estado") == 2) {
                tCambioEstado.setVisibility(View.INVISIBLE);
                bEditarPefil.setVisibility(View.INVISIBLE);
                bResetPass.setVisibility(View.INVISIBLE);
            } else {
                tCambioEstado.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callback.editarEstado(alumno, position);
                    }
                });

                bEditarPefil.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callback.cambioPerfil(alumno, position);
                    }
                });

                bResetPass.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        callback.resetPass(alumno, position);
                    }
                });
            }


        }


    }


}
