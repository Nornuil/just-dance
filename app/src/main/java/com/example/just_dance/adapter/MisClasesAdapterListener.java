package com.example.just_dance.adapter;


import com.example.just_dance.model.MisClasesModel;

public interface MisClasesAdapterListener {
    void itemClick(MisClasesModel clase, int position);
}
