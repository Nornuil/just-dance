package com.example.just_dance;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.just_dance.adapter.MisClasesAdapter;
import com.example.just_dance.model.MisClasesModel;
import com.example.just_dance.retrofit.misClases.MisClasesAPI;
import com.example.just_dance.retrofit.misClases.MisClasesCallback;
import com.example.just_dance.retrofit.response.MisClasesResponse;
import com.example.just_dance.utils.CustomProgressDialog;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MisClasesActivity extends AppCompatActivity {

    @BindView(R.id.recyclerMisClases)
    RecyclerView recyclerMisClases;
    private CustomProgressDialog progressDialog = new CustomProgressDialog();
    private MisClasesAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mis_clases);
        ButterKnife.bind(this);

        recyclerMisClases.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new MisClasesAdapter(new ArrayList<>(), this::itemMisClaseSelect);
        recyclerMisClases.setAdapter(adapter);
        progressDialog.show(MisClasesActivity.this, "Cargando...");


        new MisClasesAPI().getMisClases(this, new MisClasesCallback() {
            @Override
            public void onSuccess(MisClasesResponse response) {
                adapter.updateData(response.getMisClases());

                progressDialog.dismiss();

            }

            @Override
            public void onError() {
                progressDialog.dismiss();
                Toast.makeText(MisClasesActivity.this, "Hubo un error", Toast.LENGTH_LONG).show();
                finish();
            }

            @Override
            public void onNetworkError() {
                Toast.makeText(MisClasesActivity.this, "El dispositivo no esta conectado a la red", Toast.LENGTH_LONG).show();
                progressDialog.dismiss();
                finish();
            }
        });

    }


    private void itemMisClaseSelect(MisClasesModel clase, int position) {

        Log.e("ListaClase", "item: " + position);
    }
}